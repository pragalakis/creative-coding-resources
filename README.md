# [deprecated] Creative Coding Resources

The repo is moved to codeberg: https://codeberg.org/evasync/creative-coding-resources

A collection of resources around creative coding and generative art.

## Libraries

- [three.js](https://github.com/mrdoob/three.js/) - JavaScript 3D library.
- [regl](https://github.com/regl-project/regl) - Functional WebGL.
- [canvas-sketch](https://github.com/mattdesl/canvas-sketch) - A framework for making generative artwork in JavaScript and the browser.
- [Stackgl](http://stack.gl/) - Open software ecosystem for WebGL, built on top of browserify and npm.
- [Paper.js](http://paperjs.org/) - The swiss army knife of vector graphics scripting.
- [Pixi.js](http://www.pixijs.com/) - HTML5 2D rendering engine that uses webGL with canvas fallback.
- [p5.js](https://p5js.org/) - JavaScript library that starts with the original goal of Processing.
- [Pts.js](https://ptsjs.org/) - JavaScript library for visualization and creative-coding.
- [Fabric.js](http://fabricjs.com/) - Javascript canvas library, SVG-to-canvas parser.
- [Maker.js](https://maker.js.org) - Parametric line drawing for SVG, CNC & laser cutters.
- [OpenJSCAD](https://openjscad.org) - Programmatic 3D modeling in JavaScript.
- [Sketch.js](http://soulwire.github.io/sketch.js/) - Minimal JavaScript creative coding framework.
- [Two.js](https://two.js.org/) - Two-dimensional drawing api geared towards modern web browsers.
- [ClayGL](http://claygl.xyz/) - WebGL graphic library for building scalable Web3D applications.
- [Proton](https://github.com/a-jie/Proton) - A lightweight and powerful javascript particle engine.
- [lightgl.js](https://github.com/evanw/lightgl.js) - A lightweight WebGL library.
- [picogl.js](https://github.com/tsherif/picogl.js) - A minimal WebGL 2 rendering library.
- [Alfrid](https://github.com/yiwenl/Alfrid) - A WebGL tool set.
- [Babylon.js](https://github.com/BabylonJS/Babylon.js) - complete JavaScript framework for building 3D games with HTML 5 and WebGL.
- [twigl](https://github.com/greggman/twgl.js) - A Tiny WebGL helper Library.
- [luma.gl](https://github.com/uber/luma.gl) - WebGL2 Components for Data Visualization.
- [css-doodle](https://css-doodle.com/) - A web component for drawing patterns with CSS.

## Courses

- [The nature of code](https://natureofcode.com/) - Course by Processing Foundation
- [Interactivity & Computation](http://cmuems.com/2018/60212f/) - Course by CMU School of Art, Fall 2018 - Prof. Golan Levin / TA: Char Stiles
- [The Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw) - Youtube channel with tutorials made by Daniel Shiffman
- [The book of shaders](https://thebookofshaders.com/) - E-book by Patricio Gonzalez Vivo and Jen Lowe
- [Shader School](https://github.com/stackgl/shader-school) - A workshopper for GLSL shaders and graphics programming
- [WebGL Workshop](https://github.com/stackgl/webgl-workshop) - The sequel to shader-school: Learn the WebGL API

## Tutorials

- [Making a WebGL globe](https://substack.net/globe-tutorial/) - Tutorial by Substack
- [Beautifully animate points with WebGL and REGL](https://peterbeshai.com/blog/2017-05-26-beautifully-animate-points-with-webgl-and-regl/) - Tutorial by Peter Beshai
- [How I built a wind map with WebGL](https://blog.mapbox.com/how-i-built-a-wind-map-with-webgl-b63022b5537f?gi=1508755ea49e) - Tutorial by Vladimir Agafonkin
- [Advanced Creative Coding: WebGL and Shaders](https://github.com/mattdesl/workshop-webgl-glsl/) - Tutorial by Matt Desl
- [PROCJAM tutorials](http://www.procjam.com/tutorials/) - PROCJAM tutorials
- [Learn WebGL](https://learnwebgl.brown37.net/)

## Talks

- [Poetic Computation](https://www.youtube.com/watch?v=bmztlO9_Wvo&t=387s) - Talk by Zach Lieberman
- [Creating generative art with Javascript](https://www.youtube.com/watch?v=tJ49bTJ6fbs) - Talk by Kate Compton
- [Generative Art Speedrun](https://www.youtube.com/watch?v=4Se0_w0ISYk) - Talk by Tim Holman
- [Aesthetics + Computation: When Design Goes Retro](https://www.youtube.com/watch?v=0BblQk5L4fA) - Talk by John Maeda
- [Mapping Imaginary Cities](https://www.youtube.com/watch?v=Ic_5gRVTQ_k) - Talk by Mouse Reeve
- [Fun with webgl](https://www.youtube.com/watch?v=Aq7diSfU9Cc) - Talk by James Halliday (Substack)
- [There is also canvas](https://slideslive.com/38898318/there-is-also-canvas) - Talk by Bruno Imbrizi
- [A Box of Chaos: The Generative Artist's Toolkit](https://www.youtube.com/watch?v=kZNTozzsNqk) - Talk by Benjamin Kovach
- [Code goes in, Art comes out](https://www.youtube.com/watch?v=LBpqoj2nOQo) - Talk by Tyler Hobbs
- [Creative Coding = unexplored territories](https://www.youtube.com/watch?v=JW7oAbLVNJE) - Talk by Tim Rodenbröker
- [Radical Digital Painting](https://www.youtube.com/watch?v=htI1K37l7q0) - Talk by Jeffrey Alan Scudder on 35C3
- [Pseudofractals! Accidental aesthetics where math meets pixels](https://www.youtube.com/watch?v=NoqQQwP1Duo) - Talk by Jes Wolfe

## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [Evangelos Pragalakis](https://evangelos.dev) has waived all copyright and related or neighboring rights to this work.
